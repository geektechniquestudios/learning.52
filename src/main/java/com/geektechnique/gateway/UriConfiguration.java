package com.geektechnique.gateway;

import org.springframework.boot.context.properties.ConfigurationProperties;

// tag::uri-configuration[]
@ConfigurationProperties
class UriConfiguration {
    
    private String httpbin = "http://httpbin.org:80";

    public String getHttpbin() {
        return httpbin;
    }

    public void setHttpbin(String httpbin) {
        this.httpbin = httpbin;
    }
}
// end::uri-configuration[]
// end::code[]